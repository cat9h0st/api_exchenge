import cursesmenu

class Menu():
    #NOTE!!!
    #
    # ssf its a callback function for subscribe all twitter chanels from file
    # ssi its a callback function for subscibe to a single twitter chanel from input
    #
    #
    def __init__(self, g_info=None, g_f_info=None):
        self.g_info = g_info
        self.g_f_info = g_f_info

    @staticmethod
    def intro():
        print("""
                          .'\   /`.
                        .'.-.`-'.-.`.
                   ..._:   .-. .-.   :_...
                 .'    '-.(o ) (o ).-'    `.
                :  _    _ _`~(_)~`_ _    _  :
               :  /:   ' .-=_   _=-. `   ;\  :
               :   :|-.._  '     `  _..-|:   :
                :   `:| |`:-:-.-:-:'| |:'   :
                 `.   `.| | | | | | |.'   .'
                   `.   `-:_| | |_:-'   .'
                     `-._   ````    _.-'
                         ``-------''
               """)



    def menu(self):
        menu = cursesmenu.CursesMenu('CLI')

        INFO = cursesmenu.items.FunctionItem("get info from endpoint", self.g_info)
        INFO_FULL = cursesmenu.items.FunctionItem("get full info", self.g_f_info)

        menu.append_item(INFO)
        menu.append_item(INFO_FULL)

        return menu