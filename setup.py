from setuptools import setup, find_packages

VERSION = '0.0.1a'


setup(name='libex', version=VERSION, author='Well ATS',
      # author_email='cat@9h0st.ru',
      url="git.ittop.tech/cat9h0st/lib_api_exchange",
      install_requires=['tqdm', 'requests', 'websocket-client', 'autobahn', 'pusherclient', 'osa', 'zeep', 'click', 'curses-menu'],
      description='Python3-based API Framework',
      license='MIT',  classifiers=['Development Status :: 0 - ALPHA',
                                   'Intended Audience :: Developers'],
)