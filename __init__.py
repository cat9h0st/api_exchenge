# coding=utf-8
import INTERFACES
import pprint
from menu import Menu
import time
pp = pprint.PrettyPrinter(width=40, compact=True)
interface = INTERFACES.PE()

def get_info():
    inp = input('enter endpoint: ')
    pp.pprint(interface.private_void(inp))
    out = input('press ENTER to continue...')
    if out:
        return True

def get_full_info():
    pp.pprint(interface.full_private_void('items_data_get'))
    out = input('press ENTER to continue...')
    if out:
        return True

menu = Menu(g_info=get_info, g_f_info=get_full_info)

if __name__ == '__main__':
    menu.intro()
    time.sleep(1)
    menu.menu().show()