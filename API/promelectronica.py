import requests
import hashlib
from tqdm import tqdm

class PE():
    def __init__(self, login='Альфалидер', password='FKA1306', customer_id="109777"):
        self.version = 'v0.1'
        result = hashlib.md5(password.encode())
        self.md5 = result.hexdigest()
        self.data = {
            "login":login,
            "password": self.md5,
            "customer_id" : customer_id
            }
        self.uri = 'https://proliant2.promelec.ru:224/rpc_test'

    def info(self):
        return 'version = %s' % self.version

    def get_resonse(self, endpoint, options={}):
        data = self.data
        data['method'] = endpoint
        if options:
            for i in options:
                data[i] = options[i]
        res = requests.post(self.uri, json=data)
        return res.json()

    def get_full_resonse(self, endpoint, options={}):
        data = self.data
        data['method'] = endpoint
        if options:
            for i in options:
                data[i] = options[i]
        res = requests.post(self.uri, json=data).json()
        i = 0
        ret = []
        pbar = tqdm(total=len(res) + 1)
        while i < len(res):
            insert = {}
            cur_id = res[i]['item_id']
            newdata = self.data
            newdata['method'] = 'item_data_get'
            newdata['item_id'] = cur_id
            try:
                resource = requests.post(self.uri, json=data).json()
            except Exception as e:
                print(e)
                resource = None
            if resource:
                insert['origin'] = res[i]
                insert['additional'] = resource
                ret.append(insert)
            pbar.update(1)
            i += 1
        pbar.close()
        return ret
